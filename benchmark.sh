#!/bin/bash

for filename in `ls -v ./example_benchmark/*.txt`; do
    name=${filename##*/}
    echo "Testing $name"

    { time for ((i=0; i<5; i++)); do
        $1 "./example_benchmark/$name" "./example_benchmark/b/$name"
    done } |& grep real
    echo ""
done
