Installation
============

Downloading
-----------
To install the software you must first have git and GCC installed and have a web connection. Go to the project's home page on [Bitbucket](https://bitbucket.org/chattanoogawildcats/alpha-seaweed/) and use `git clone` to clone the repository (see the command below). You should now have a new directory, `alpha-seaweed`, which will contain everything you need to install the project.

    $ git clone https://bitbucket.org/chattanoogawildcats/alpha-seaweed.git

Building
--------

The build system allows the user to build different version of the software depending on the execution environment. To compile the divide and conquer implementation to run on an MPI enabled cluster, use `make mpi` and run the `mpi` binary. For execution on a single processor machine, `make fast` should be used with the `fast` executable. If AVX2 is not available on the target machine, `make serial` will compile a version that runs serially, without the use of AVX2.

Running the MPI version of the software on Tinis involves the use of another script. The `mpijob.pbs` file provided is a job script that can be submitted to Tinis for execution using Moab/Slurm. The command below shows how to use the msub command to submit the job. The included pbs script script can be modified to change the runtime options such as number of processes to use and maximum allowed running time.

    $ msub mpijob.pbs

To make these files available from the command line in any directory, copy the standalone executable to your `bin` directory in your home directory (recommended option), or add the file's location to your path. If you have not previously set up a bin directory, use the commands listed below to do so.

    $ # Add the bin folder to your path
    $ mkdir ~/bin
    $ echo '\nPATH=$PATH:~/bin\n' >> ~/.bashrc

    $ # Copy the built executable into your bin folder
    $ cp ~/directory/to/alpha-seaweed/fast ~/bin/

Note that this method relies upon using the bash shell. If you are using a different shell, or are running Windows without Cygwin, you will need to adjust the above steps accordingly.

Using the software
==================

When one runs the executable with no options it will print out the correct way to use it and find the LCS scores for two included example files. The arguments that must be passed into the executable are the names of the two files that contain the strings whose LCS scores we wish to compute. Optionally one can also pass the name of a file to output runtime logging information to (see the instructions given by the executable).

Note that the dynamic programming solution relies upon the AVX2 instruction set; however if your machine does not have AVX2 the program can still be run for development through a virtual machine. Although not designed for this purpose, Valgrind is included with many Linux distributions and can be used to run the code for small debugging data sets (such as the ones included in example/asmall.txt and example/bsmall.txt).
