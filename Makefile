ROOT=$(dir $(lastword $(MAKEFILE_LIST)))

UNAME = $(shell uname)

ifeq ($(UNAME), Linux)
CC=g++
endif
ifeq ($(UNAME), Darwin)
CC=clang-omp++
endif

SOURCES=$(wildcard src/*.cpp)
OBJECTS = $(patsubst src/%.cpp, obj/%.o, $(SOURCES))
MPI_FOLDER = $(patsubst src/MPI/%.cpp, obj/MPI/%.o, $(wildcard src/MPI/*.cpp))
MPI_OBJECTS = $(patsubst obj/%.o, obj/MPI/%.o, $(OBJECTS)) $(MPI_FOLDER) # take any normal files that are not already included as MPI files
TESTS = $(wildcard test/*.cpp)
TESTOBJECTS = $(patsubst test/%.cpp, obj/%.o, $(SOURCES))

NEWVECEX = -msse4 -mavx -march=core-avx2
VECEX = -msse -msse2 -msse3 $(NEWVECEX)

CPPFLAGS   = -c -Wall -Werror -Wextra -std=c++11 -pedantic -fopenmp $(VECEX)
LDFLAGS    = -fopenmp -std=c++11
DEBUGFLAGS = -Og -g -fno-inline
FASTFLAGS  = -O3 -march=native -mfpmath=sse -ftree-vectorizer-verbose=1
TESTFLAGS  = -lboost_unit_test_framework

.SECONDARY:


main: CPPFLAGS += $(DEBUGFLAGS)
main: LDFLAGS  += $(DEBUGFLAGS)
main: $(OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $^

mpi: CPPFLAGS += $(FASTFLAGS) -DWITH_MPI
mpi: LDFLAGS  += $(FASTFLAGS) -DWITH_MPI
mpi: CC=mpiicpc
mpi: $(MPI_OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $^

fast: CPPFLAGS += $(FASTFLAGS)
fast: LDFLAGS  += $(FASTFLAGS)
fast: $(OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $^

sloppy: CPPFLAGS = -c -std=c++11 -fopenmp $(VECEX)
sloppy: $(OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $^

serial: CPPFLAGS := $(filter-out $(NEWVECEX), $(CPPFLAGS))
serial: $(OBJECTS)
	$(CC) $(LDFLAGS) -o $@ $^

benchmark: CPPFLAGS += $(DEBUGFLAGS) -pg
benchmark: $(OBJECTS)
	$(CC) $(LDFLAGS) $(DEBUGFLAGS) -pg -o benchtemp $^
	./benchtemp
	gprof benchtemp > $@
	rm benchtemp

tests: $(basename $(notdir $(TESTS)))

obj/%Test.o: test/%Test.cpp
	$(CC) $(CPPFLAGS) -o $@ $<

# If the MPI version exists, use it; else use regular version
obj/MPI/%.o: src/MPI/%.cpp
	$(CC) $(CPPFLAGS) -o $@ $<

obj/MPI/%.o: src/%.cpp
	$(CC) $(CPPFLAGS) -o $@ $<

obj/%.o: src/%.cpp
	$(CC) $(CPPFLAGS) -o $@ $<

%Test: testexe/%Test
	./$^

testexe/%Test: obj/%Test.o $(filter-out obj/Main.o, $(OBJECTS))
	$(CC) $(LDFLAGS) $^ -o $@ $(TESTFLAGS)

clean:
	-@rm -f obj/*.o
	-@rm -f obj/MPI/*.o
	-@rm -f mpi
	-@rm -f main
	-@rm -f sloppy
	-@rm -f serial
	-@rm -f fast
	-@rm -f gmon.out
	-@rm -f benchmark
	-@rm -f testexe/*
	
