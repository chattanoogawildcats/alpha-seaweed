#include <fstream>
#include <cstdlib>
#include <cstddef>
#include <algorithm>
#include <iostream>

#ifdef WITH_MPI
#include "mpi.h"
#endif

#include "MPI/MpiBraid.hpp"
#include "Braid.hpp"
#include "Divide.hpp"
#include "Matrix.hpp"
#include "SeaweedUtil.hpp"

using namespace std;

int main(int argc, char *argv[]) {
    string aSrc, bSrc;


    // Program has been run incorrectly, or the developer wants to run files a.txt and b.txt
    if (argc == 1) {
        SeaweedUtil::showCorrectUsage();
        cout << endl << "\tWill now run with two given example strings, ./example/a.txt and ./example/b.txt" << endl << endl;
        
        aSrc = "./example/a.txt";
        bSrc = "./example/b.txt";
    } else if (argc == 2) {
        SeaweedUtil::exitWithErrorMessage("ERROR: Not enough arguments");
        exit(EXIT_FAILURE);

    } else {
        aSrc = argv[1];
        bSrc = argv[2];
    }

    // Stop this from being destroyed early by declaring it here
    ofstream log;

    if (argc > 3) {
        // Loop over remaining arguments
        for (auto i = 3; i < argc; i++) {
            string arg(argv[i]);

            if (arg.find("-c") != string::npos) {
                // Number of cores
                SeaweedUtil::setNumberOfCores(arg);
            }
            else if (arg.find("-l=") != string::npos) {
                // Set a file to log to
                log.open(arg.substr(3), ios::app); // Open logging file and append
            }
            else if (arg.find("help") != string::npos) {
                cout << "\"help\" detected in argument, printing out help message" << endl;
                SeaweedUtil::showCorrectUsage();
            }
            else {
                cerr << "WARNING: Unrecognized argument \"" << arg << "\"" << endl;
            }
        }
    }


    // Set up logging, either to no-where or to our file output
    clog.rdbuf(log.rdbuf());
    clog << endl << "\t====" << SeaweedUtil::getCurrentTimeString() << "====" << endl << endl;

#ifdef WITH_MPI
    // If the program is compiled with MPI, use this version

    // Initialise MPI. Code between here and MPI_Finalize is parallel
    MPI::Init(argc, argv);

    PreComputeStore store("precompute_5.bin", 5);

    MPI::COMM_WORLD.Barrier();
    double startTime = MPI::Wtime();
    MPI::COMM_WORLD.Barrier();

    MpiBraid mpiBraid = MpiBraid(aSrc, bSrc, store);
    mpiBraid.run();

    if (mpiBraid.isBaseProcessor()){
        double endTime = MPI::Wtime();

        cout << "LCS: " << mpiBraid.getLcs() << endl;
    }

    MPI::Finalize();

#else
    uint64 startTime = SeaweedUtil::curTime();

    // If the program is compiled without MPI, use the single processor braid
    Braid braid = Braid(SeaweedUtil::readFile(aSrc), SeaweedUtil::readFile(bSrc));
    braid.run();

    uint64 endTime = SeaweedUtil::curTime();

    cout << "LCS: " << braid.getLcs() << endl;
    cout << "Running time: " << (endTime - startTime) / 1000.0L << "s" << endl;

#endif // #ifdef WITH_MPI

    // Cleanup for logging file
    clog << "\tRun finished" << endl;
    clog.flush();
    clog.rdbuf(NULL);

    return 0;
}
