#include <cmath>
#include <algorithm>
#include <stdio.h>
#include "PreComputeStore.hpp"
#include "Permutation.hpp"
#include "Matrix.hpp"

/**
 * Empty constructor, will set size to 0.
 */
PreComputeStore::PreComputeStore(){
    size = 0;
}

/**
 * Creates a new storage for pre-computed matrix multiplication data. 
 * Given a maximum size, this method computes the results from all possible
 * permutation multiplications for permutation matrices up to the maximum size.
 * 
 * A filename can be provided and the method will attempt to load from that file
 * before performing the pre-computations and saving to the file if it doesn't exist.
 * 
 * @param inputFile The name of the file to load pre-computed data from
 * @param maxSize The maximum size of the input matricies to pre-compute.
 */
PreComputeStore::PreComputeStore(const char * inputFile, size_t maxSize){
    // Will read input, or create a new one if it doesn't exist yet
    if (!load(inputFile)){
        initWithSize(maxSize);
        save(inputFile);
    }
}

/**
 * Creates a new storage without a filename parameter.
 *
 * @param maxSize The maximum size of the input matricies to pre-compute.
 */
PreComputeStore::PreComputeStore(size_t maxSize){
    initWithSize(maxSize);
}

/**
 * Called from the constructors to initialise the pre-computation storage up to a given size.
 *
 * @param maxSize The maximum size of the input matricies to pre-compute.
 */
void PreComputeStore::initWithSize(size_t maxSize){
    PreComputeStore emptyStore;
    size = maxSize;

    // Pre-compute the matrix multiplications
    for (size_t i = 2; i <= maxSize; i++){
        int *permValues = new int[i];
        std::vector<Permutation> perms;

        for (size_t j = 0; j < i; j++){
            permValues[j] = j;
        }

        do{
            perms.push_back(Permutation(permValues, permValues + i));
        }while(std::next_permutation(permValues, permValues + i));

        for (size_t j = 0; j < perms.size(); j++){
            for (size_t k = 0; k < perms.size(); k++){
                Matrix aMatrix = Matrix(perms[j]);
                Matrix bMatrix = Matrix(perms[k]);

                Matrix result = aMatrix.multiply(bMatrix, 0, emptyStore);

                insert(perms[j], perms[k], result.toPermutation());
            }
        }

        delete[] permValues;
    }
}

/**
 * Save the pre-computed results to a file. If the file already exists,
 * this method will not overwrite it.
 * 
 * @param outputFileName The name of the file to save to.
 * @return true if the file was written to successfully, false otherwise.
 */
bool PreComputeStore::save(const char * outputFileName){
    FILE* outputFile = fopen(outputFileName, "wbx");

    if (outputFile == NULL){
        return false;
    }

    // Write the maximum size
    fwrite(&size, sizeof(size_t), 1, outputFile);

    // Write the size of the main array
    size_t storeSize = store.size();
    fwrite(&storeSize, sizeof(size_t), 1, outputFile);

    // Write an array of pre-computations
    for (size_t i = 0; i < storeSize; i++){
        // Write the size of this entry
        size_t entrySize = store[i].size();
        fwrite(&entrySize, sizeof(size_t), 1, outputFile);

        // Write this entry
        for (size_t j = 0; j < entrySize; j++){
            size_t permSize = store[i][j].size();
            fwrite(&permSize, sizeof(size_t), 1, outputFile);

            fwrite(&store[i][j][0], sizeof(int), permSize, outputFile);
        }

    }

    fclose(outputFile);

    return true;
}

/**
 * Load the pre-computed results from a file. 
 * 
 * @param inputFileName The name of the file to load from.
 * @return true if the file exists and was read from successfully, false otherwise.
 */
bool PreComputeStore::load(const char * inputFileName){
    FILE* inputFile = fopen(inputFileName, "rb");

    if (inputFile == NULL){
        return false;
    }

    // Read maximum size
    fread(&size, sizeof(size_t), 1, inputFile);

    // Read size of main array
    size_t storeSize;
    fread(&storeSize, sizeof(size_t), 1, inputFile);

    store.resize(storeSize);

    for (size_t i = 0; i < storeSize; i++){
        // Read size of this entry
        size_t entrySize;
        fread(&entrySize, sizeof(size_t), 1, inputFile);

        store[i].resize(entrySize);

        for (size_t j = 0; j < entrySize; j++){
            size_t permSize;
            fread(&permSize, sizeof(size_t), 1, inputFile);

            store[i][j].resize(permSize);

            fread(&store[i][j][0], sizeof(int), permSize, inputFile);
        }
    }

    fclose(inputFile);

    return true;
}

/**
 * Insert a result into the store. Calculates the indexes for the two input permutations
 * and inserts the result into the correct vector location. Resizes the vectors if needed.
 * 
 * @param permA The first permutation that makes the result.
 * @param permB The second permutation that makes the result.
 * @param result The permutation that results from multiplying permA and permB.
 */
void PreComputeStore::insert(Permutation permA, Permutation permB, Permutation result){
    size_t indexA = getPermIndex(permA);
    size_t indexB = getPermIndex(permB);

    if (store.size() < indexA){
        store.resize(indexA + 1);
    }

    if (store[indexA].size() < indexB){
        store[indexA].resize(indexB + 1);
    }

    store[indexA][indexB] = result;
}

/**
 * Calculates the unique index for a permutation.
 * If the maximum permutation size for this store is k, then this method
 * uses a base-k number counting system to generate an index.
 *
 * @param perm The permutation whose index we want to calculate.
 * @return The permutation's unique index.
 */
size_t PreComputeStore::getPermIndex(Permutation perm){
    size_t index = 0;

    for (size_t i = 0; i < perm.size(); i++){
        size_t columnBase = (size_t) pow(size, i);

        index += columnBase * perm[i];
    }

    return index;
}


/**
 * Takes two permutations as input and gets their multiplied result from the store.
 * This method computes the unique indexes of the permutations then gets the result.
 *
 * @param permA The first permutation.
 * @param permB The second permutation.
 * @return The result of permA * permB or permB * permA.
 */
Permutation PreComputeStore::get(Permutation permA, Permutation permB){
    size_t indexA = getPermIndex(permA);
    size_t indexB = getPermIndex(permB);

    return get(indexA, indexB);
}

/**
 * Takes two indexes and attempts to find a result in the store. 
 * This method checks in both directions for a result. If a result
 * is not found, the empty permutation is returned.
 *
 * @param indexA The first permutation index.
 * @param indexB The second permutation index.
 * @return The resulting permutation, or the empty permutation if one is not found.
 */
Permutation PreComputeStore::get(size_t indexA, size_t indexB){
    if (indexA < store.size() && store[indexA].size() > indexB){
        return store[indexA][indexB];
    }

    if (indexB < store.size() && store[indexB].size() > indexA){
        return store[indexB][indexA];
    }

    Permutation empty;
    return empty;
}
