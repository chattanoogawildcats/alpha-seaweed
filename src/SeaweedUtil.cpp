#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <ctime>
#include <omp.h>
#include <sys/time.h>

#include "SeaweedUtil.hpp"
#include "Permutation.hpp"
#include "Matrix.hpp"

using namespace std;

/**
 * Prints out the correct usage of the software
 */
void SeaweedUtil::showCorrectUsage() {
    cout << endl << "\tCorrect usage (with file1 and file2 containing your two input strings, N is the number of cores you wish to use):" << endl 
        << "\t\t" << "./lcs" << " file1 file2 ";
    cout << "(-cN -l=logFileName)" << endl << endl;
}

/**
 * Prints an error, then exits
 * @param message The error message to display
 */
void SeaweedUtil::exitWithErrorMessage(string message) {
    showCorrectUsage();
    cerr << message << endl;
    
    exit(EXIT_FAILURE);
}

/** 
 * Reads an entire file into a string. This will eat all of your ram for big strings!
 * @param fileName The name of the file to read
 * @return The contents of the file as a string
 * 
 */
string SeaweedUtil::readFile(string fileName) {
    ifstream fileStream(fileName.c_str());

    if (!fileStream.good()) {
        stringstream fmt;
        fmt << "ERROR: File \"" << fileName << "\" is not accessible. (Does it exist? Do I have the permissions to access it?)";
        exitWithErrorMessage(fmt.str());
    }

    fileStream.seekg(0, fileStream.end);
    size_t fileLength = fileStream.tellg();
    
    string fileContents(fileLength, ' ');

    fileStream.seekg(0);
    fileStream.read(&fileContents[0], fileLength);

    return fileContents;
}

/**
 * Sets the number of cores, or prints an error and exits
 * @param arg The entire argument as passed in by the user
 */
void SeaweedUtil::setNumberOfCores(string arg) {
    string numCores = arg.substr(2); // Cut off -c

    try {
        int cores = stoi(numCores); // Throws exception on bad input
        omp_set_dynamic(0);
        omp_set_num_threads(cores);
        # pragma omp parallel
        {
            if (omp_get_thread_num() == 0) {
                clog << "Using " << omp_get_num_threads() << " threads." << endl;
            }
        }
    }
    catch (exception &) {
        exitWithErrorMessage("ERROR: Invalid number of cores specified");
    }
}

/**
 * Gets the current time as a formatted string.
 * 
 * @return The current time string.
 */
string SeaweedUtil::getCurrentTimeString() {
    time_t t = time(0);
    char buf[80];
    struct tm * now = localtime(&t);
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", now);
    return string(buf);
}

/**
 * Get the size of an input file.
 * @param fileSrc The location of the file
 * @return The length of the file
 */
std::streampos SeaweedUtil::getFileSize(std::string fileSrc){
    std::ifstream inputStream(fileSrc.c_str());
    inputStream.seekg(0, inputStream.end);

    return inputStream.tellg();
}

/**
 * Returns the LCS score for an input permuation 
 * @param p An implicitly represented permutation 
 * @param l The length of input string b
 * @return The LCS score for the input permuation
 */
unsigned int SeaweedUtil::getLcs(Permutation p, unsigned int l){
    int count = 0;
    int d = p.size() - l;

    for(unsigned int i = d; i < p.size(); i++) {
        if (p[i] < (int) l) {
            count++;
        }
    }

    return l - count;
}

/**
 * Returns the LCS score for an input permuation matrix
 * @param m An implicitly represented permutation matrix
 * @param l The length of input string b
 * @return The LongestCommonSubsequence score for the input matrix
 */
unsigned int SeaweedUtil::getLcs(Matrix m, unsigned int l) {
    Permutation p = m.toPermutation();
    return getLcs(p, l);
}

/**
 * Return the current UNIX time in ms
 * 
 * @return The current UNIX time in ms
 */
uint64 SeaweedUtil::curTime(){
    struct timeval tv;

    gettimeofday(&tv, NULL);

    uint64 ret = tv.tv_usec;
    /* Convert from micro seconds (10^-6) to milliseconds (10^-3) */
    ret /= 1000;

    /* Adds the seconds (10^0) after converting them to milliseconds (10^-3) */
    ret += (tv.tv_sec * 1000);

    return ret;
}
