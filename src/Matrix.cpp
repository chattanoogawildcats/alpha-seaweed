#include "Matrix.hpp"
#include "Permutation.hpp"
#include "PreComputeStore.hpp"
#include <math.h>
#include <stdlib.h>

#define EMPTY -1

using namespace std;

Matrix::Matrix(){
    permSize = 0;
}

/**
 * Construct a matrix with the specified permutation vector.
 * Store the permutation as an internal variable, and its size.
 * Store the transposed for O(1) access later in the multiply.
 * @param p Input permutation vetor
 */
Matrix::Matrix(Permutation p) {
    perm = p;
    permSize = p.size();
    transposed = transpose(p);
}

/**
 * Return the permutation vector that implicitly represents this matrix.
 * @return The permutation vector
 */
Permutation Matrix::toPermutation() {
    return perm;
}

/**
 * Find the transpose of a permutation vector.
 * @param p Input permutation vector
 * @return Transposed vector
 */
Permutation Matrix::transpose(Permutation p) {
    Permutation trans;
    trans.resize(p.size());

    preComputeIndex = 0;
    for (size_t i = 0; i < trans.size(); i++) {
        trans[i] = EMPTY;

        if (getSize() <= 5){
            size_t columnBase = (size_t) pow(5, i);

            preComputeIndex += columnBase * p[i];
        }
    }

    for (size_t i = 0; i < trans.size(); i++) {
        if (p[i] != EMPTY){
            trans[p[i]] = i;
        }
    }

    return trans;
}

/**
 * Swap the internal vector and transposed.
 */
void Matrix::swap() {
    Permutation temp = this->perm;
    this->perm = this->transposed;
    this->transposed = temp;
}

/**
 * Prints the permutation matrix in a human-readable way.
 * Includes the input (top/left) edges and how they map to the bottom/right edges.
 * @param across The number of elements that run left-to-right.
 */
void Matrix::printSeaweed(size_t across){
    cout << across << endl;
}

/**
 * Prints the permutation matrix in a human-readable way.
 * Prints the permutation matrix, where 0s are represented as '.' characters for clarity.
 * @param across The number of elements that run left-to-right.
 */
void Matrix::printPermutation(size_t across){
    int down = getSize() - across;

    for (int i = 0; i < down; i++){
        cout << "|";

        for (size_t j = 0; j < across; j++){
            if (perm[i * across + j] == i){
                cout << " 1 ";
            }else{
                cout << " . ";
            }
        }

        cout << "|" << endl;
    }
}

/**
 * Prints the permutation matrix in a human-readable way.
 * Simply prints a space-separated list of the elements in the permutation vector.
 */
void Matrix::printVector(){
    cout << "[";

    for (size_t i = 0; i < getSize(); i++){
        cout << " " << perm[i] << " ";
    }

    cout << "]";
}

/**
 * The size of the matrix, currently stored as member, not a call to perm.size()
 * @return The size of the matrix
 */
size_t Matrix::getSize(){
    return permSize;
}

/**
 * Multiplies the permutation matrix (A) by input matrix (B), both of size n in O(nlog(n))
 * @param matrix Input permutation matrix to multiply by
 * @return Matrix result of the multiplication of size n
 */
Matrix Matrix::multiply(Matrix matrix, size_t horizontalLength, PreComputeStore & store) {
    // Basis case: [1] * [1] == [1]
    if (getSize() == 1) {
        return *this;
    }

    if (getSize() <= store.size){
        Permutation val = store.get(preComputeIndex, matrix.preComputeIndex);

        if (!val.empty()){
           return Matrix(val);
        }
    }

    // Determine how much we need to implicitly grow each matrix by

    size_t aGrow = 0;
    size_t bGrow = 0;

    if (horizontalLength){
        aGrow = matrix.getSize() - horizontalLength;
        bGrow = getSize() - horizontalLength;
    }

    // The implicit size of the A matrix, given that it has to grow
    size_t implicitSize = getSize() + aGrow;
    size_t size = implicitSize;

    int half = size / 2;

    Permutation aLo, aHi, aLoLocs, aHiLocs;
    //Set the sizes vectors to build the subproblems into
    aLo.resize(half);
    aHi.resize(size - half);
    aLoLocs.resize(half);
    aHiLocs.resize(size - half);

    int aLoCount = 0;
    int aHiCount = 0;
    
    //Creation of the subproblems
    //Iterate over the rows in matrix A
    for (uint row = 0; row < size; row++) {
        int valueToCompare = row;

        if (row >= aGrow){
            valueToCompare = perm[row - aGrow] + aGrow;
        }

        //For all non-zeros on the left half of A
        if (valueToCompare < half) {
            //Store the column value in the subproblem 
            aLo[aLoCount] = valueToCompare;
            //Store the row value to reconstruct the problem
            aLoLocs[aLoCount] = row;
            aLoCount++;
            
        //For all non-zeroes on the right half of A
        } else {
            //Store the new column value in the subproblem
            aHi[aHiCount] = valueToCompare - half;
            //Store the row value to reconstruct the problem
            aHiLocs[aHiCount] = row;
            aHiCount++;
        }
    }

    size_t implicitSizeB = matrix.getSize() + bGrow;
    size = implicitSizeB;
    half = size / 2;

    Permutation bLo, bHi, bLoLocs, bHiLocs;
    //Set the sizes vectors to build the subproblems into
    bLo.resize(half);
    bHi.resize(size - half);
    bLoLocs.resize(half);
    bHiLocs.resize(size - half);

    int bLoCount = 0;
    int bHiCount = 0;

    //Iterate over the columns in matrix B (iterate over the transposed rows)
    for (uint column = 0; column < size; column++) {
        int valueToCompare = column;

        if (column < matrix.getSize()){
            valueToCompare = matrix.transposed[column];
        }

        //For all non-zeroes in the top half of B
        if (valueToCompare < half) {
            //Store the row value in the subproblem
            bLo[bLoCount] = valueToCompare;
            //Store the column value to reconstruct the problem
            bLoLocs[bLoCount] = column;
            bLoCount++;
            
        //For all non-zeroes in the bottom half of B
        } else {
            //Store the row value in the subproblem
            bHi[bHiCount] = valueToCompare - half;
            //Store the column value to reconstruct the problem
            bHiLocs[bHiCount] = column;
            bHiCount++;
        }
    }

    //Construct each subproblem as its own matrix from the permutation vectors built above
    Matrix mALo = Matrix(aLo);
    Matrix mAHi = Matrix(aHi);
    Matrix mBLo = Matrix(bLo);
    Matrix mBHi = Matrix(bHi);

    //This swap is required to adjust between row/column based representations
    mBLo.swap();
    mBHi.swap();

    //Make recursive calls on the subproblems
    Matrix cLo = mALo.multiply(mBLo, 0, store);
    Matrix cHi = mAHi.multiply(mBHi, 0, store);

    // Red and blue matrices of size n x n
    size = implicitSize;
    Permutation red, blue;

    //Set the sizes for the vectors to reconstruct the problem into
    red.resize(size);
    blue.resize(size);

    int cLoCount = 0;
    int cHiCount = 0;

    //Reconstruction of the subproblems
    
    //Iterate over the rows in the red matrix
    for (uint i = 0; i < size; i++) {
        //If there was a non-zero in the i-th row of ALo, then there will be a non-zero in the i-th row of red
        if (cLoCount < (int)aLoLocs.size() //We must guard against indexing out of the bounds of the subproblem
            && (int)i == aLoLocs[cLoCount]) {
            //Store in the i-th row a non-zero in equivalent column of BLo
            red[i] = bLoLocs[cLo.perm[cLoCount]];
            cLoCount++;
        
        //There was a zero in the i-th row of ALo
        } else {
            //Store an empty row 
            red[i] = EMPTY;
        }
    }

    //Iterate over the rows in the blue matrix 
    for (uint i = 0; i < size; i++) {
        //If there was a non-zero in the i-th row of AHi, then there willl be a non-zero in the i-th row of red 
        if (cHiCount < (int)aHiLocs.size() //We must guard against indexing out of the bounds of the subproblem
            &&(int)i == aHiLocs[cHiCount]) {
            //Store in the i-th row a non-zero in equivalent column of BHi
            blue[i] = bHiLocs[cHi.perm[cHiCount]];
            cHiCount++;
        
        //There was a zero in the i-th row of AHi
        } else {
            //Store an empty row
            blue[i] = EMPTY;
        }
    }

    // Now run ANTS on cRed and cBlue
    Path redPath, bluePath;

    //Construct a red and blue matrix from the permutation vectors created
    Matrix mRed = Matrix(red);
    Matrix mBlue = Matrix(blue);

    //Construct a red and blue path through the matrices 
    redPath = computeRedPath(mRed, mBlue);
    bluePath = computeBluePath(mRed, mBlue);

    Permutation c;
    //Set the size of a permutation vector to store the result
    c.resize(implicitSize);

    //Iterate over the rows of the result matrix adding the red and blue non-zeroes to the same matrix
    for (uint i = 0; i < size; i++) {
        //If there is a non-zero in the i-th row of the red matrix
        if (red[i] != EMPTY) {
            //Store the non-zero in the i-th row of the result
            c[i] = red[i];
        //There will be a non-zero in the i-th row of the blue matrix
        } else {
            //Store the non-zero in the i-th row of the result
            c[i] = blue[i];
        }
    }

    // Add the green bullet intersections
    // skip the trivial bottom-left and top-right cells
    
    //Iterate over the length of the paths, ignoring the bottom-left and top-right cells
    //We want to find intersections between the red and blue paths that occur on the integer grid
    for (int i = 1; i < (int) redPath.size() - 1; i++){
        if (redPath[i].first == bluePath[i].first //Intersection on the x-coordinates at step i
            && redPath[i].second == bluePath[i].second //Intersection on the y-coordinates at step i
            && redPath[i].first == fabs(floor(redPath[i].first)) //Check each coordinates is on the integer grid
            && redPath[i].second == fabs(floor(redPath[i].second)) //Check each coordinates is on the integer grid
            && bluePath[i].first == fabs(floor(bluePath[i].first)) //Check each coordinates is on the integer grid
            && bluePath[i].second == fabs(floor(bluePath[i].second))) //Check each coordinates is on the integer grid
        {
            //Perform the correcting swap
            c[redPath[i].first] = redPath[i].second;
        }
    }
    
    //Construct a matrix from the result permutation vector 
    return Matrix(c);;
}

/**
 * Computes deltaAbove for the cell above us.
 * @param red Red matrix
 * @param blue Blue matrix
 * @param aboveRow Row above the current one
 * @param aboveColumn Column above the current one
 * @param deltaAbove Old deltaAbove value we're updating
 * @return New deltaAbove
 */
inline float Matrix::computeDeltaAbove(Matrix red, Matrix blue, float aboveRow, float aboveColumn, float deltaAbove) {
    if (aboveRow < red.getSize() && aboveRow >= 0 && red.perm[int (aboveRow)] != EMPTY && red.perm[int (aboveRow)] > aboveColumn) {
        deltaAbove--;
    }

    if (aboveRow < blue.getSize() && aboveRow >= 0 && blue.perm[int (aboveRow)] != EMPTY && blue.perm[int (aboveRow)] <= aboveColumn) {
        deltaAbove--;
    }

    return deltaAbove;
}

/**
 * Computes deltaRight for the cell to our right.
 * @param red Red matrix
 * @param blue Blue matrix
 * @param rightRow Row to the right of the current one
 * @param rightColumn Column to the right of the current one
 * @param deltaRight Old deltaRight value we're updating
 * @return New deltaRight
 */

inline float Matrix::computeDeltaRight(Matrix red, Matrix blue, float rightRow, float rightColumn, float deltaRight) {
    if (rightColumn < red.getSize() && rightColumn >= 0 && red.transposed[int (rightColumn)] != EMPTY && red.transposed[int (rightColumn)] >= rightRow) {
        deltaRight++;
    }

    if (rightColumn < blue.getSize() && rightColumn >= 0 && blue.transposed[int (rightColumn)] != EMPTY && blue.transposed[int (rightColumn)] < rightRow) {
        deltaRight++;
    }

    return deltaRight;
}

Path Matrix::computeRedPath(Matrix red, Matrix blue) {
    Path redPath;

    float row = red.getSize() - 0.5; // floats for 1/2-integer indexing scheme
    float column = -0.5;
    float delta = 0; // # of top-left blues - # of bottom-right reds

    float aboveColumn, aboveRow;
    float rightColumn, rightRow;
    float deltaAbove = 0;
    float deltaRight = 0;

    bool onHalfRow = true;
    bool onHalfColumn = true; // are we on the 1/2-integer grid? (We start there in the bottom-left corner)

    // Red path
    while (row != -0.5 || column != red.getSize() - 0.5) { // until we hit the top-right corner
         // Add the current cell to the path
        redPath.push_back( pair<float, float>(row, column) );

        // Are we on the 1/2-integer grid?
        onHalfRow = row != fabs(floor(row));
        onHalfColumn = column != fabs(floor(column));

        // Possible next cell (above and right) coordinates
        aboveRow = row - 0.5;
        aboveColumn = column;

        rightRow = row;
        rightColumn = column + 0.5;

        // Set deltas
        deltaAbove = delta;
        deltaRight = delta;

        // Compute deltaAbove
        if (onHalfRow){
            deltaAbove = computeDeltaAbove(red, blue, aboveRow, aboveColumn, deltaAbove);
        }

        // Compute deltaRight
        if (onHalfColumn){
            deltaRight = computeDeltaRight(red, blue, rightRow, rightColumn, deltaRight);
        }

        // Move
        if (row == -0.5){ // we're on the top row
            delta = deltaRight;
            column += 0.5; // go right
        } else {
            if (deltaAbove == 0 && deltaRight == 0){ // if both balanced
                delta = deltaAbove;
                row -= 0.5; // go up by default (red path)
            } else if (deltaAbove != 0 && deltaRight != 0) { // if both unbalanced
                delta = deltaAbove;
                row -= 0.5; // go up by default (red path)
            } else if (deltaAbove == 0 && deltaRight != 0) { // if up is balanced
                delta = deltaAbove;
                row -= 0.5; // go up
            } else if (deltaAbove != 0 && deltaRight == 0) { // if right is balanced
                delta = deltaRight;
                column += 0.5; // go right
            }
        }
    }

    // Top-right corner
    redPath.push_back( pair<float, float>(row, column) );
    return redPath;
}

Path Matrix::computeBluePath(Matrix red, Matrix blue) {
    Path bluePath;

    float row = blue.getSize() - 0.5; // floats for 1/2-integer indexing scheme
    float column = -0.5;
    float delta = 0; // # of top-left blues - # of bottom-right reds

    float aboveColumn, aboveRow;
    float rightColumn, rightRow;
    float deltaAbove = 0;
    float deltaRight = 0;

    bool onHalfRow = true;
    bool onHalfColumn = true; // are we on the 1/2-integer grid? (We start there in the bottom-left corner)

    while (row != -0.5 || column != blue.getSize() - 0.5) { // until we hit the top-right corner
         // Add the current cell to the path
        bluePath.push_back( pair<float, float>(row, column) );

        // Are we on the 1/2-integer grid?
        onHalfRow = row != fabs(floor(row));
        onHalfColumn = column != fabs(floor(column));

        // Possible next cell (above and right) coordinates
        aboveRow = row - 0.5;
        aboveColumn = column;

        rightRow = row;
        rightColumn = column + 0.5;

        // Set deltas
        deltaAbove = delta;
        deltaRight = delta;

        // Compute deltaAbove
        if (onHalfRow){
            deltaAbove = computeDeltaAbove(red, blue, aboveRow, aboveColumn, deltaAbove);
        }

        // Compute deltaRight
        if (onHalfColumn){
            deltaRight = computeDeltaRight(red, blue, rightRow, rightColumn, deltaRight);
        }

        // Move
        if (column == blue.getSize() - 0.5){ // we're on the right column
            delta = deltaAbove;
            row -= 0.5; // go up
        } else {
            if (deltaAbove == 0 && deltaRight == 0){ // if both balanced
                delta = deltaRight;
                column += 0.5; // go right by default (blue path)
            } else if (deltaAbove != 0 && deltaRight != 0) { // if both unbalanced
                delta = deltaRight;
                column += 0.5; // go right by default (blue path)
            } else if (deltaAbove == 0 && deltaRight != 0) { // if up is balanced
                delta = deltaAbove;
                row -= 0.5; // go up
            } else if (deltaAbove != 0 && deltaRight == 0) { // if right is balanced
                delta = deltaRight;
                column += 0.5; // go right
            }
        }
    }

    // Top-right corner
    bluePath.push_back( pair<float, float>(row, column) );

    return bluePath;
}
