#ifndef BRAID_HPP
#define BRAID_HPP

#include <string>
#include <iostream>
extern "C" 
{
#include <pmmintrin.h>
#include <immintrin.h>
}

#include "SeaweedCombing.hpp"
#include "Permutation.hpp"
#include "Matrix.hpp"

typedef unsigned int Seaweed;

class Braid : public SeaweedCombing {
    private:
        char *a, *b;
        size_t aLen, bLen;
        Permutation braidPermutation;

    public:
        Braid(std::string a, std::string b);
        ~Braid();
        
        // Public for the sake of testing
        void comb(const char, const char, const Seaweed, const Seaweed, Seaweed *, Seaweed * );
        void comb(const char *, const  char *, const Seaweed *, const Seaweed *, Seaweed *, Seaweed *);

        void buildBraidToVerticalMax(const unsigned int, Seaweed *&, Seaweed *&, Seaweed *&, Seaweed *&);
        void buildBraidToHorizontalMax(const unsigned int, Seaweed *&, Seaweed *&, Seaweed *&, Seaweed *&);
        void buildBraidToEnd(const unsigned int, Seaweed *&, Seaweed *&, Seaweed *&, Seaweed *&);
        void buildBraid();
        void print();
        void print(std::ostream&);
        void run();
        size_t getBraidLen();
        Permutation getPermutation();
        Matrix getPermutationMatrix();
        size_t getShortStrLen();
        size_t getLongStrLen();
        unsigned int getLcs();
};

#endif /* BRAID_HPP */
