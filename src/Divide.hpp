#ifndef DIVIDE_HPP
#define DIVIDE_HPP

#include <string>
#include <vector>
#include <tuple>
#include "SeaweedCombing.hpp"
#include "Matrix.hpp"
#include "PreComputeStore.hpp"

class Divide : public SeaweedCombing {
private:
    std::string shortStr, longStr;
    Matrix result;
    PreComputeStore preComputeStore;
public:
    Divide(std::string a, std::string b);
    
    Matrix divide(std::string a, std::string b);
    Matrix buildPa();
    Matrix buildPb();
    void run();
    unsigned int getLcs();

    // static unsigned int begin(std::string, std::string);
    // static Matrix divide(std::string, std::string);
    // static Matrix implicitMultiply(Matrix, Matrix, size_t);
    // static Matrix buildPa(std::string, std::string);
    // static Matrix buildPb(std::string, std::string);
    // static Matrix growA(Matrix, unsigned int);
    // static Matrix growB(Matrix, unsigned int);
    // static unsigned int LongestCommonSubsequence(Matrix, unsigned int);
};

#endif /* DIVIDE_HPP */
