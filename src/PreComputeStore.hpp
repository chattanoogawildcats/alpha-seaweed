#ifndef PRE_COMPUTE_STORE_HPP
#define PRE_COMPUTE_STORE_HPP

#include <utility>
#include "Permutation.hpp"

typedef std::vector<std::vector<Permutation>> PreComputeMap;

class PreComputeStore{
    public:
        size_t size;
        PreComputeMap store;

        PreComputeStore();
        PreComputeStore(const char *, size_t);
        PreComputeStore(size_t maxSize);
        void initWithSize(size_t maxSize);
        Permutation get(Permutation permA, Permutation permB);
        void insert(Permutation permA, Permutation permB, Permutation result);
        size_t getPermIndex(Permutation perm);
        Permutation get(size_t indexA, size_t indexB);

        bool save(const char * outputFileName);
        bool load(const char * inputFileName);

    private:
    
};

#endif /* PRE_COMPUTE_STORE_HPP */
