#ifndef SEAWEED_UTIL_HPP
#define SEAWEED_UTIL_HPP

#include <string>

#include "Permutation.hpp"
#include "Matrix.hpp"

typedef long long int64; typedef unsigned long long uint64;

class SeaweedUtil{
    public:
        static void showCorrectUsage();
        static void exitWithErrorMessage(std::string message);
        static std::string readFile(std::string fileName);
        static std::streampos getFileSize(std::string);
        static void setNumberOfCores(std::string arg);
        static std::string getCurrentTimeString();
        static unsigned int getLcs(Permutation p, unsigned int l);
        static unsigned int getLcs(Matrix m, unsigned int l);
        static uint64 curTime();

};

#endif /* SEAWEED_UTIL_HPP */
