#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <iostream>
#include <vector>
#include <map>
#include <list>
#include "Permutation.hpp"
#include "PreComputeStore.hpp"

typedef std::vector <std::pair <float, float>> Path;

class Matrix {
    public:
        size_t permSize;
        Permutation perm;
        Permutation transposed;
        size_t preComputeIndex;
        
        Matrix();
        Matrix(Permutation);
        Permutation toPermutation();
        Matrix multiply(Matrix, size_t, PreComputeStore&);
        size_t getSize();
        void printSeaweed(size_t across);
        void printPermutation(size_t across);
        void printVector();


    private:
        Path computeRedPath(Matrix, Matrix);
        Path computeBluePath(Matrix, Matrix);
        float computeDeltaAbove(Matrix, Matrix, float, float, float);
        float computeDeltaRight(Matrix, Matrix, float, float, float);
        Permutation transpose(Permutation);
        void swap();
};

#endif /* MATRIX_HPP */
