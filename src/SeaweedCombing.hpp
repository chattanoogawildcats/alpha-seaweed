#ifndef SEAWEED_COMBING_HPP
#define SEAWEED_COMBING_HPP

#include "Permutation.hpp"

class SeaweedCombing{
    public:
        virtual ~SeaweedCombing() {}
        virtual void run() = 0;
        virtual unsigned int getLcs() = 0;
};

#endif /* SEAWEED_COMBING_HPP */