#include <fstream>
#include <mpi.h>
#include <string>
#include <cmath>
#include <sstream>

#include "MpiHelper.hpp"

/**
 * Read a chunk of a file based on which MPI process we are. When all processes run this on the same file they get their own section of the file.
 * @param fileSrc The location of the file we wish to read.
 * @return A std::string with the chunk that was read.
 */
std::string MpiHelper::readSectionOfFile(std::string fileSrc){
    int numProcessors;
    int thisId;

    numProcessors = MPI::COMM_WORLD.Get_size();
    thisId = MPI::COMM_WORLD.Get_rank();

    // Get the length of the larger file
    std::ifstream inputStream(fileSrc.c_str());
    inputStream.seekg(0, inputStream.end);
    unsigned int fileLength = inputStream.tellg();

    unsigned int chunkSize = ceil(fileLength / (float) numProcessors);
    unsigned int thisProcessorFilePos = chunkSize * thisId;

    // Move to the correct position in the file
    inputStream.seekg(thisProcessorFilePos, inputStream.beg);

    // If we are on the 'final' processor, make sure to get all of the remaining file
    if (thisId == numProcessors - 1){
        chunkSize = fileLength - ((numProcessors - 1) * chunkSize);
    }

    std::string chunkString(chunkSize, ' ');
    inputStream.read(&chunkString[0], chunkSize);

    return chunkString;
}

/**
 * Gets the message size from a given processor with a give tag in a given communicator.
 * 
 * @param processorId The processor we are receiving from.
 * @param tag The tag of the message.
 * @param comm The communicator we are receiving in.
 * @return The size of the message.
 */
int MpiHelper::getResponseSize(int processorId, int tag){
    MPI::Status status;

    MPI::COMM_WORLD.Probe(processorId, tag, status);
    int responseSize = status.Get_count(MPI::INT);

    return responseSize;
}
