#include <string>
#include <cmath>

#include "mpi.h"
#include "MpiBraid.hpp"
#include "MpiHelper.hpp"
#include "../SeaweedUtil.hpp"
#include "../PreComputeStore.hpp"
#include "../Permutation.hpp"
#include "../Matrix.hpp"
#include "../Braid.hpp"
#include "../Divide.hpp"

/**
 * A constructor for the MpiBraid object.
 * An instance of this object should be created on each processor. The object takes
 * two input files and calculates their LCS using MPI to divide the work.
 * 
 * @param shortFileSrc The first input file.
 * @param longFileSrc The second input file.
 */
MpiBraid::MpiBraid(std::string shortFileSrc, std::string longFileSrc, PreComputeStore pstore){
    store = pstore;
    numProcessors = MPI::COMM_WORLD.Get_size();
    processorId = MPI::COMM_WORLD.Get_rank();

    // Get the lengths of each input file
    std::streampos shortLength = SeaweedUtil::getFileSize(shortFileSrc);
    std::streampos longLength = SeaweedUtil::getFileSize(longFileSrc);

    // Make sure the variable names accurately describe the inputs
    if (shortLength > longLength){
        std::swap(shortFileSrc, longFileSrc);
    }

    std::string fileChunk = MpiHelper::readSectionOfFile(longFileSrc);
    std::string shortStr = SeaweedUtil::readFile(shortFileSrc);

    horizontalLength = shortStr.length();
    Braid localBraid(fileChunk, shortStr);
    localBraid.run();
    localPermutationMatrix = localBraid.getPermutationMatrix();
}

/**
 * Gets the local permutation matrix.
 * 
 * @return The current local permutation matrix.
 */
Matrix MpiBraid::getPermutationMatrix(){
    return localPermutationMatrix;
}

/**
 * Gets the local processor ID.
 * 
 * @return The processor ID.
 */
int MpiBraid::getProcessorId(){
    return processorId;
}

/**
 * Returns true if this processor is the 'base processor'.
 * Here, we define that as processor with ID 0.
 * 
 * @return true if this processor has ID 0, false otherwise.
 */
bool MpiBraid::isBaseProcessor(){
    return processorId == 0;
}

/**
 * Runs the code that performs any MPI communication to calculate the final permutation matrix
 * We model the matrix multiplication on a binary tree. In each layer of the tree, pairs of 
 * processors will exchange a permutation matrix and combine the results.
 * In total, we have log(p) levels for p processors.
 * 
 * This method contains an MPI barrier at the end. As such, it will not return until all
 * processors have finished executing.
 */
void MpiBraid::run(){
    int numLevels = ceil(log2(numProcessors));    


    // Iterate over each level of the tree
    for (int level = 1; level <= numLevels; level++){
        int jumpSize = pow(2, level);
        int neighbourDistance = jumpSize / 2; // Distance to nearest neighbour

        if (!receivePermutation(jumpSize, neighbourDistance)){
            // If we are not a receiver, check if we are a sender
           sendPermutation(jumpSize, neighbourDistance);
        }
    }
}

/**
 * Checks if the local processor should receive data in this level and receives it.
 * Then multiplies the local matrix with the received matrix.
 * 
 * @param jumpSize The difference between receiving processor IDs.
 * @param neighbourDistance The distance to the sending neighbour.
 * @return true if this is a receiving processor, false otherwise
 */
bool MpiBraid::receivePermutation(int jumpSize, int neighbourDistance){
    if (processorId % jumpSize == 0){
        // We are a receiving processor
        MPI::Status status;
        int receiveId = processorId + neighbourDistance;

        // Check the processor exists
        if (receiveId < numProcessors){
            // Get the size of the response
            int responseSize = MpiHelper::getResponseSize(receiveId, 0);
            
            Permutation receivedPerm;
            receivedPerm.resize(responseSize);

            MPI::COMM_WORLD.Recv(&receivedPerm[0], responseSize, MPI::INT, receiveId, 0, status);

            Matrix receivedMatrix(receivedPerm);

            localPermutationMatrix = localPermutationMatrix.multiply(receivedMatrix, horizontalLength, store);
        }

        return true;
    }

    return false;
}

/**
 * Checks if the local processor should send data in this level and sends it if so.
 * 
 * @param jumpSize The difference between receiving processor IDs.
 * @param neighbourDistance The distance to the sending neighbour.
 */
void MpiBraid::sendPermutation(int jumpSize, int neighbourDistance){
    if (processorId % jumpSize == neighbourDistance){
        // We are a sending processor
        int sendId = processorId - neighbourDistance;
        Permutation localBraidPerm = localPermutationMatrix.toPermutation();

        MPI::COMM_WORLD.Send(&localBraidPerm[0], localBraidPerm.size(), MPI::INT, sendId, 0);
    }
}

/**
 * Uses the Divide class to calculate the longest common subsequence based on
 * the local permutation matrix.
 * 
 * @return The LCS score for the local permutation matrix.
 */
unsigned int MpiBraid::getLcs(){
    return SeaweedUtil::getLcs(localPermutationMatrix, horizontalLength);
}
