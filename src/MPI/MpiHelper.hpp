#ifndef MPI_HELPER_HPP
#define MPI_HELPER_HPP

#include "mpi.h"
#include <string>

/*
 * Class that contains a series of helper functions for when operating in MPI
 */

class MpiHelper {
    public:
        static std::string readSectionOfFile(std::string);
        static int getResponseSize(int, int);

};

#endif /* MPI_HELPER_HPP */
