#ifndef MPI_BRAID_HPP
#define MPI_BRAID_HPP

#include <string>

#include "../Matrix.hpp"
#include "../Matrix.hpp"
#include "../PreComputeStore.hpp"


class MpiBraid {
    public:
        Matrix localPermutationMatrix;
        size_t horizontalLength;
        int numProcessors;
        int processorId;
        PreComputeStore store;

        MpiBraid(std::string, std::string, PreComputeStore);
        Matrix getPermutationMatrix();
        void run();
        unsigned int getLcs();
        int getProcessorId();
        bool isBaseProcessor();

    private:
        bool receivePermutation(int, int);
        void sendPermutation(int, int);
};


#endif /* MPI_BRAID_HPP */
