#include <string>
#include <vector>
#include <cmath>
#include <tuple>
#include <iostream>
#include "Divide.hpp"
#include "Matrix.hpp"
#include "Permutation.hpp"
#include "SeaweedUtil.hpp"

using namespace std;

/**
 * Create a new Divide object with the input strings.
 * 
 * @param a The first string to compute the LCS for.
 * @param b The second string to comput the LCS for.
 */
Divide::Divide(string a, string b){
    if (a.length() > b.length()) {
        swap(a, b);
    }

    shortStr = a;
    longStr = b;
}

/**
 * Begin executing the divide and conquer algorithm.
 */
void Divide::run(){
    result = divide(shortStr, longStr);
}

/**
 * Recursively divide the inputs and compute their permutation matrices. 
 * Recombines these matrices using Matrix::multiply

 * @param a The first string to divide (should be shortest)
 * @param b The second string to divide (should be longest)
 * @return The multiplied Matrix result
 */
Matrix Divide::divide(string a, string b) {
    if (a.length() > 2) {
        string aDash = a.substr(0, ceil(a.length() / 2.0));
        string aDashDash = a.substr(ceil(a.length() / 2.0), a.length());

        Matrix pa = divide(aDash, b);
        Matrix pb = divide(aDashDash, b);
        
        return pa.multiply(pb, b.length(), preComputeStore);
    } else if (a.length() == 2) {
        string aDash = a.substr(0, 1);
        string aDashDash = a.substr(1, 1);
        
        Matrix pa = buildPa();
        Matrix pb = buildPb();

        return pa.multiply(pb, b.length(), preComputeStore);
    } else {
        return buildPb();
    }
}

/**
 * Return the LCS score for the result computed by this Divide object
 */
unsigned int Divide::getLcs(){
    return SeaweedUtil::getLcs(result, longStr.length());
}

/**
 * Here we build a permuation matrix for two input strings
 * String a will be a single character
 * This should be similar to initialising the top row of a seaweed matrix
 * Matrix will be represented implicitly as a 1D vector for spatial sanity
 * I think vector will be of length b.length + 2 to account for those braids added
 * This should run in O(n)
 *
 * @param a Input string a - will be a single character
 * @param b Input string b
 * @return Implicitly represented permutation matrix for the input
 */
Matrix Divide::buildPa(){
    Permutation bottom, right, res;

    bottom.resize(longStr.length()+2);
    right.resize(longStr.length()+2);
    res.resize(longStr.length()+2);

    res[0]    = 0;
    bottom[0] = 0;
    right[0]  = 1;

    for (size_t i = 1; i < longStr.length() + 1; i++) {
        if(longStr[i - 1] == shortStr[0]) {
            bottom[i] = right[i - 1];
            right[i] = i + 1;
        } else {
            bottom[i] = i + 1;
            right[i] = right[i - 1];
        }

        res[bottom[i]] = i;
    }

    bottom[bottom.size() - 1] = right[bottom.size() - 2];
    res[bottom[bottom.size() - 1]] = bottom.size() - 1;

    return Matrix(res);
}

/**
 * @param a Input string a - will be a single character
 * @param b Input string b
 * @return Implicitly represented permutation matrix for the input
 */
Matrix Divide::buildPb(){
    Permutation bottom, right, res;

    bottom.resize(longStr.length() + 2);
    right.resize(longStr.length() + 2);
    res.resize(longStr.length() + 2);

    if (longStr[0] == shortStr[0]) {
        right[0] = 1;
        bottom[0] = 0;
    } else {
        right[0] = 0;
        bottom[0] = 1;
    }

    res[bottom[0]] = 0;

    for (size_t i = 1; i < longStr.length(); i++) {
        if(longStr[i] == shortStr[0]) {
            bottom[i] = right[i - 1];
            right[i] = i + 1;
        } else {
            bottom[i] = i + 1;
            right[i] = right[i - 1];
        }

        res[bottom[i]] = i;
    }

    bottom[bottom.size() - 2] = right[bottom.size() - 3];
    res[bottom[bottom.size() - 2]] = bottom.size() - 2;

    res[bottom.size() - 1] = bottom.size() - 1;

    return Matrix(res);
}
