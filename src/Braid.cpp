#include <utility>
#include <string>
#include <vector>
#include <omp.h>
#include <algorithm>

#include <cstring>

#include <iostream>

extern "C" 
{
#include <pmmintrin.h>
#include <immintrin.h>
}

#if defined(__INTEL_COMPILER)
#include <malloc.h>
#else
#include <mm_malloc.h>
#endif // defined(__GNUC__)

#include "Braid.hpp"
#include "Divide.hpp"
#include "SeaweedUtil.hpp"

#define ALIGNMENT 32
using namespace std;

/*
 *  In the seaweed table, going from top-left to bottom-right.
 *
 *   b-->
 *  a
 *  ^
 *  |
 *
 * In memory in there are two "braid" objects, a and b which are vectors that run parallel to their respective input strings above. 
 * Each cell in the braid contains a pair of ints, first = starting index of the seaweed exiting the bottom of the cell, second = starting index of the seaweed exiting the right of the cell.
 *
 * The aim of the braid objects is to implicitly represent the entire seawead matrix using only a linear amount of memory. Once built it will contain the starting indexes for the seaweeds reaching the bottom row and right hand column of the seaweed matrix. From this we can obtain the semi-local LCS scores in a linear amount of time!
 *
 * Each grid square has it's own number, starting from 0 going right along the top. The first cell also starts at the length of string b going down along the left hand side.
 * Each seaweed is identified by the square from which it originated.
*/

/**
 * Constructor for a braid.
 * Takes two strings as arguments and creates the vectors that will store their
 * braids.
 * @param x the first string to use.
 * @param y the second string to use.
 */
Braid::Braid(string x, string y) {
    if (x.length() > y.length()) {
        swap(x, y); // Allows for more spatial locality, go along longest string
    }

    a = (char *) _mm_malloc((x.length() + 1) * sizeof(char), ALIGNMENT);
    b = (char *) _mm_malloc((y.length() + 1) * sizeof(char), ALIGNMENT);
    aLen = x.length();
    bLen = y.length();

    reverse(x.begin(), x.end());

    strcpy(a, x.c_str());
    strcpy(b, y.c_str());
}

Braid::~Braid() {
    _mm_free(a);
    _mm_free(b);
}

/**
 * This function sets up the braid's memory and runs the buildBraid function, making the braid ready to receive results
 */
void Braid::run() {
    // Initialise the memory that will contain the resulting braid
    braidPermutation.resize(getBraidLen());
    buildBraid();
}

/*
 * This will use the notation described on p47 of the book
 * Each function takes two arguments that are bounds to part of the Braid vector, whilst the other two bounds are implicit from the function semantics
 * For the vertical braid A of length m, the lower bound will be lLower, the upper bound lUpper, with lLower,lUpper in [0,m]
 * For the horizontal braid B of lenghth n, the lower bound will be iLower, the upper bound iUpper, with iLower,iUpper in [0,n]
 * These functions DO NOT return the LCS score!!!
 */

/**
 * Takes the characters from string a and b corresponding to the current cell along with the seaweeds that are entering this cell
 * and 'combs' them. This means the seaweeds will exit the cell in the correct order based on if they cross or not.
 * 
 * For efficiency, this method avoids the use of branching. Instead, a condition mask is used to choose between two values.
 * See the following, and simplify http://graphics.stanford.edu/~seander/bithacks.html#ConditionalSetOrClearBitsWithoutBranching
 * 
 * @param a the character from string a corresponding to this cell.
 * @param b the character from string b corresponding to this cell.
 * @param top the seaweed that entered this cell from the top.
 * @param left the seaweed that entered this cell from the left.
 * @param bot the seaweed that will exit this cell to the bottom.
 * @param right the seaweed that will exit this cell to the right.
 */
inline void Braid::comb(const char a, const char b, const Seaweed top, const Seaweed left, Seaweed * bot, Seaweed * right) {
    Seaweed cond = (top < left) || (a == b);
    Seaweed ncondMask = cond - 1;
    Seaweed condMask = ~ncondMask; //condMask = mask if cond, else ~mask.

    *bot   = (condMask & left) | (ncondMask & top);
    *right = (condMask & top)  | (ncondMask & left);
}

/**
 * Combing using AVX instructions.
 * This method performs 32 a == b comparisons in parallel and then performs 8 top < left comparisons in parallel 4 times.
 * The result is that 32 combing operations are performed per call to this method.
 * 
 * @param aBlock a char pointer to the start of a block of 32 chars that represent characters from string a.
 * @param bBlock a char pointer to the start of a block of 32 chars that represent characters from string b.
 * @param top a pointer to the start of a block of 32 Seaweeds in memory that enter the cells from the top.
 * @param top a pointer to the start of a block of 32 Seaweeds in memory that enter the cells from the left.
 * @param top a pointer to the start of a block of 32 Seaweeds in memory that will exit the cells to the bottom.
 * @param top a pointer to the start of a block of 32 Seaweeds in memory that will exit the cells to the right.
 */
inline void Braid::comb(const char * aBlock, const char * bBlock, const Seaweed * top, const Seaweed * left, Seaweed * bot, Seaweed * right) {
#ifndef __AVX2__
    // If AVX2 features are not supported, do 32 serial combs
    for (int i = 0; i < 32; i++){
        comb(aBlock[i], bBlock[i], top[i], left[i], &bot[i], &right[i]);
    }
#else
    __m256i aVec = _mm256_loadu_si256((__m256i *) aBlock);
    __m256i bVec = _mm256_loadu_si256((__m256i *) bBlock);

    // Perform 32 a == b operations
    __m256i aEqB = _mm256_cmpeq_epi8(aVec, bVec);

    // Unpack the 8 bit comparison integers into 16 bit integers
    __m256i aEqBhi =  _mm256_unpackhi_epi8(aEqB, aEqB);
    __m256i aEqBlo =  _mm256_unpacklo_epi8(aEqB, aEqB);

    // Unpack into 32 bit vectors
    __m256i aEqBhilo = _mm256_unpacklo_epi16(aEqBhi, aEqBhi);
    __m256i aEqBhihi = _mm256_unpackhi_epi16(aEqBhi, aEqBhi);

    __m256i aEqBlolo = _mm256_unpacklo_epi16(aEqBlo, aEqBlo);
    __m256i aEqBlohi = _mm256_unpackhi_epi16(aEqBlo, aEqBlo);

    // Permute vectors to restore original data ordering
    __m256i loloPerm = _mm256_permute2x128_si256(aEqBlolo, aEqBlohi, 0x20);
    __m256i lohiPerm = _mm256_permute2x128_si256(aEqBhilo, aEqBhihi, 0x20);
    __m256i hiloPerm = _mm256_permute2x128_si256(aEqBlolo, aEqBlohi, 0x31);
    __m256i hihiPerm = _mm256_permute2x128_si256(aEqBhilo, aEqBhihi, 0x31);

    // Array of the unpacked vectors
    __m256i aEqBArr[] = {loloPerm, lohiPerm, hiloPerm, hihiPerm};

    // Load 4 sets of 8 top and left values
    __m256i topVectors[4];
    __m256i leftVectors[4];
    for (int i = 0; i < 4; i++){
        topVectors[i] = _mm256_loadu_si256((__m256i *)  &top[i * 8]);
        leftVectors[i] = _mm256_loadu_si256((__m256i *) &left[i * 8]);
    }

    // Perform 4 sets of 8 top < left comparisons
    __m256i botVectors[4];
    __m256i rightVectors[4];
    for (int i = 0; i < 4; i++){
        __m256i topLtLeft = _mm256_cmpgt_epi32 (leftVectors[i], topVectors[i]);
        __m256i condMask = _mm256_or_si256(topLtLeft, aEqBArr[i]);
        
        __m256i botResLeft  = _mm256_and_si256(condMask, leftVectors[i]);
        __m256i botResRight = _mm256_andnot_si256(condMask, topVectors[i]);
        botVectors[i] = _mm256_or_si256(botResLeft, botResRight);

        __m256i rightResLeft  = _mm256_and_si256(condMask, topVectors[i]);
        __m256i rightResRight = _mm256_andnot_si256(condMask, leftVectors[i]);
        rightVectors[i] = _mm256_or_si256(rightResLeft, rightResRight);
    }

    // Store the results into the bot and right Seaweed arrays
    for (int i = 0; i < 4; i++){
        _mm256_storeu_si256((__m256i*) &bot[i * 8], botVectors[i]);
        _mm256_storeu_si256((__m256i*) &right[i * 8], rightVectors[i]);
    }
#endif /* __AVX2__ */
}

/**
 * Builds the braid in increasing anti-diagonal sizes until the maximum diagonal length has been reached.
 * 
 * @param maxDiagLen the maximum diagonal length.
 * @param prevDiagBot the array that the bottom component of the final diagonal calculated will be placed in.
 * @param prevDiagRight the array that right component of the final diagonal calculated will be placed in.
 */
void Braid::buildBraidToVerticalMax(const unsigned int maxDiagLen, Seaweed * &prevDiagBot, Seaweed * &prevDiagRight, Seaweed * &curDiagBot, Seaweed * &curDiagRight){
    // Index of the final element in string a
    const unsigned int aEnd = aLen - 1;

    // Load first element into prevDiag arrays
    comb(a[aEnd], b[0], aLen, aEnd, &prevDiagBot[0], &prevDiagRight[0]);

    for (unsigned int i = 1; i < maxDiagLen; i++){
        unsigned int diagLength = i + 1;
        unsigned int startIndexA = aLen - diagLength;
        
        // We don't have an entry in prevDiagRight for the first element in the diagonal, so treat this is a special case
        comb(a[startIndexA], b[0], prevDiagBot[0], startIndexA, &curDiagBot[0], &curDiagRight[0]);

        // Only use AVX for diagonals that are big enough
        if (diagLength > 34){
            unsigned int unrollJ = ((diagLength - 1) / 32) * 32;
            unsigned int j;

            for (j = 1; j < unrollJ; j += 32){
                comb(&a[startIndexA + j], &b[j], &prevDiagBot[j], &prevDiagRight[j - 1], &curDiagBot[j], &curDiagRight[j]);
            }

            // Cleanup loop
            for (; j < diagLength - 1; j++){
                comb(a[startIndexA + j], b[j], prevDiagBot[j], prevDiagRight[j - 1], &curDiagBot[j], &curDiagRight[j]);
            }
        }else{
            for (unsigned int j = 1; j < diagLength - 1; j++){
                comb(a[startIndexA + j], b[j], prevDiagBot[j], prevDiagRight[j - 1], &curDiagBot[j], &curDiagRight[j]);
            }
        }

        // We also don't have an entry in prevDiagBot for the last element, so do that special case
        comb(a[aEnd], b[i], aLen + i, prevDiagRight[i - 1], &curDiagBot[i], &curDiagRight[i]);

        swap(curDiagBot, prevDiagBot);
        swap(curDiagRight, prevDiagRight);
    }
}

/**
 * Builds the diagonals of length maxDiagLen across the grid up to the maximum horizontal length.
 * This method will begin outputting the computed cell data to the result braid.
 * 
 * @param maxDiagLen the maximum diagonal length.
 * @param prevDiagBot the array that the bottom component of the final diagonal calculated will be placed in.
 * @param prevDiagRight the array that top component of the final diagonal calculated will be placed in.
 */
void Braid::buildBraidToHorizontalMax(const unsigned int maxDiagLen, Seaweed * &prevDiagBot, Seaweed * &prevDiagRight, Seaweed * &curDiagBot, Seaweed * &curDiagRight){
    unsigned int unrollJ = ((maxDiagLen - 1) / 32) * 32;

    for (unsigned int i = maxDiagLen, braidIndex = 0; i < bLen; i++, braidIndex++){
        // Write to the braid (bottom part)
        braidPermutation[prevDiagBot[0]] = braidIndex;

        // The last item in the anti-diagonal doesn't have a prevDiagBot entry, so use a special case here
        comb(a[aLen - 1], b[i], aLen + i, prevDiagRight[maxDiagLen - 1], &curDiagBot[maxDiagLen - 1], &curDiagRight[maxDiagLen - 1]);

        unsigned int j;
        for (j = 0; j < unrollJ; j += 32){
            unsigned int bIndex = braidIndex + 1 + j;

            comb(&a[j], &b[bIndex], &prevDiagBot[j + 1], &prevDiagRight[j], &curDiagBot[j], &curDiagRight[j]);
        }

        // Cleanup loop
        for (; j < maxDiagLen - 1; j++){            
            unsigned int bIndex = braidIndex + 1 + j;

            comb(a[j], b[bIndex], prevDiagBot[j + 1], prevDiagRight[j], &curDiagBot[j], &curDiagRight[j]);
        }

        swap(curDiagBot, prevDiagBot);
        swap(curDiagRight, prevDiagRight);
    }
}

/**
 * Finishes building the braid by iterating over shrinking anti-diagonals.
 * 
 * @param maxDiagLen the maximum diagonal length.
 * @param prevDiagBot the array that the bottom component of the final diagonal calculated will be placed in.
 * @param prevDiagRight the array that top component of the final diagonal calculated will be placed in.
 */
void Braid::buildBraidToEnd(const unsigned int maxDiagLen, Seaweed * &prevDiagBot, Seaweed * &prevDiagRight, Seaweed * &curDiagBot, Seaweed * &curDiagRight){
    for (
        unsigned int i = 1, braidRightIndex = aLen + bLen - 1, braidBotIndex = bLen - maxDiagLen;
        i < maxDiagLen;
        i++, braidRightIndex--, braidBotIndex++
    ){
        unsigned int diagLength = maxDiagLen - i;

        // Write to the result braid on the right and bottom edges
        braidPermutation[prevDiagBot[0]] = braidBotIndex;
        braidPermutation[prevDiagRight[maxDiagLen - i]] = braidRightIndex;

        if (diagLength > 34){
            unsigned int unrollJ = (diagLength / 32) * 32;
            unsigned int j;

            for (j = 0; j < unrollJ; j += 32){
                comb(&a[j], &b[braidBotIndex + 1 + j], &prevDiagBot[j + 1], &prevDiagRight[j], &curDiagBot[j], &curDiagRight[j]);
            }

            // Cleanup loop
            for (; j < diagLength; j++){
                comb(a[j], b[braidBotIndex + 1 + j], prevDiagBot[j + 1], prevDiagRight[j], &curDiagBot[j], &curDiagRight[j]);
            }
        }else{
            for (unsigned int j = 0; j < diagLength; j++){
                comb(a[j], b[braidBotIndex + 1 + j], prevDiagBot[j + 1], prevDiagRight[j], &curDiagBot[j], &curDiagRight[j]);
            }
        }

        swap(curDiagBot, prevDiagBot);
        swap(curDiagRight, prevDiagRight);
    }

    // Write the final element
    braidPermutation[prevDiagBot[0]] = bLen - 1;
    braidPermutation[prevDiagRight[0]] = bLen;
}

/**
 * Runs the O(mn) seaweed combing algorithm to generate the final seaweed braids.
 */
void Braid::buildBraid() {
    // The maximum diagonal length
    const unsigned int maxDiagLen = aLen;

    int diagMemSize = maxDiagLen * sizeof(Seaweed);
    Seaweed * prevDiagBot = (Seaweed *) _mm_malloc(diagMemSize, ALIGNMENT);
    Seaweed * prevDiagRight = (Seaweed *) _mm_malloc(diagMemSize, ALIGNMENT);
    Seaweed * curDiagBot = (Seaweed *) _mm_malloc(diagMemSize, ALIGNMENT);
    Seaweed * curDiagRight = (Seaweed *) _mm_malloc(diagMemSize, ALIGNMENT);

    // Build the different components of the braid
    buildBraidToVerticalMax(maxDiagLen, prevDiagBot, prevDiagRight, curDiagBot, curDiagRight);
    buildBraidToHorizontalMax(maxDiagLen, prevDiagBot, prevDiagRight, curDiagBot, curDiagRight);
    buildBraidToEnd(maxDiagLen, prevDiagBot, prevDiagRight, curDiagBot, curDiagRight);

    _mm_free(curDiagBot);
    _mm_free(curDiagRight);
    _mm_free(prevDiagBot);
    _mm_free(prevDiagRight);
}

/**
 * Prints the braid in a human readable way.
 * The default behaviour is to output to clog.
 */
void Braid::print(){
    print(clog);
}

/**
 * Prints the braid in a human readable way.
 * @param outputStream The stream to output to.
 */
void Braid::print(ostream &outputStream){
    outputStream << "[";

    for (size_t i = 0; i < bLen + aLen; i++){
        outputStream << " " << braidPermutation[i] << " ";
    }

    outputStream << "]" << endl;
}

/**
 * Returns the length of the braid object.
 * @return A size_t representing the length of the braid.
 */
size_t Braid::getBraidLen(){
    return aLen + bLen;
}

/**
 * Returns the braid represented as a permutation vector.
 * @return The Permutation vector.
 */
Permutation Braid::getPermutation(){
    return braidPermutation;
}

/**
 * Returns the braid's permutation matrix.
 * Warning: this method will create a new matrix each time it is called, so use sparingly.
 * @return The Matrix that represents this braid.
 */
Matrix Braid::getPermutationMatrix(){
    return Matrix(braidPermutation);
}

/**
 * Returns the length of the shorter input string.
 * In our notation, this is string A.
 * @return The length of the shorter string.
 */
size_t Braid::getShortStrLen(){
    return aLen;
}

/**
 * Returns the length of the longer input string.
 * In our notation, this is string B.
 * @return The length of the longer string.
 */
size_t Braid::getLongStrLen(){
    return bLen;
}

/**
 * Uses the Divide class to calculate the longest common subsequence based on
 * the local permutation matrix.
 * 
 * @return The LCS score for the local permutation matrix.
 */
unsigned int Braid::getLcs(){
    return SeaweedUtil::getLcs(braidPermutation, getLongStrLen());
}
