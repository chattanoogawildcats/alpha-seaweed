#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE MatrixTest
#include <boost/test/unit_test.hpp>

#include "../src/Matrix.hpp"

// matrixSize is the size of matrices we're testing
// p, q and expected are nxn permutation matrices represented implicitly as a vector
// e.g. the permutation vector (3 0 2 1) corresponds to the permutation matrix
// [0 0 0 1]
// [1 0 0 0]
// [0 0 0 1]
// [0 1 0 0]

void testANTS(permutation p, permutation q, permutation expected) {
    Matrix m(p);
    Matrix n(q);

    Matrix result(p.size(), p.size());
    permutation resultP = m.multiply(n).toPermutation();

    BOOST_CHECK_EQUAL_COLLECTIONS(
        resultP.begin(), resultP.end(),
        expected.begin(), expected.end()
    );
}

BOOST_AUTO_TEST_CASE( test1x1_1 ) {
    testANTS({0}, {0}, {0});
}

BOOST_AUTO_TEST_CASE( test2x2_1 ) {
    testANTS({1, 0}, {1, 0}, {1, 0});
}

BOOST_AUTO_TEST_CASE( test2x2_2 ) {
    testANTS({0, 1}, {1, 0}, {1, 0});
}

BOOST_AUTO_TEST_CASE( test2x2_3 ) {
    testANTS({1, 0}, {0, 1}, {1, 0});
}

BOOST_AUTO_TEST_CASE( test2x2_4 ) {
    testANTS({0, 1}, {0, 1}, {0, 1});
}

BOOST_AUTO_TEST_CASE( test3x3_1 ) {
    testANTS({2, 1, 0}, {2, 1, 0}, {2, 1, 0});
}

BOOST_AUTO_TEST_CASE( test3x3_2 ) {
    testANTS({1, 0, 2}, {2, 1, 0}, {2, 1, 0});
}

BOOST_AUTO_TEST_CASE( test3x3_3 ) {
    testANTS({0, 1, 2}, {0, 1, 2}, {0, 1, 2});
}

BOOST_AUTO_TEST_CASE( test3x3_4 ) {
    testANTS({1, 0, 2}, {1, 0, 2}, {1, 0, 2});
}

// Implicit permutation matrix multiplication is NOT commutative! See these two 3x3 test cases
BOOST_AUTO_TEST_CASE( test3x3_5 ) {
    testANTS({2, 0, 1}, {1, 0, 2}, {2, 1, 0});
}

BOOST_AUTO_TEST_CASE( test3x3_6 ) {
    testANTS({1, 0, 2}, {2, 0, 1}, {2, 0, 1});
}

BOOST_AUTO_TEST_CASE( test4x4_1 ) {
    testANTS({1, 3, 0, 2}, {3, 0, 2, 1}, {3, 2, 0, 1});
}

BOOST_AUTO_TEST_CASE( test5x5_1 ) {
    testANTS({0, 4, 1, 3, 2}, {4, 3, 2, 1, 0}, {4, 3, 2, 1, 0});
}

BOOST_AUTO_TEST_CASE( test5x5_2 ) {
    testANTS({2, 1, 4, 0, 3}, {3, 4, 0, 2, 1}, {4, 3, 2, 0, 1});
}

BOOST_AUTO_TEST_CASE( test6x6_1 ) {
    testANTS({3, 2, 5, 4, 0, 1}, {5, 0, 2, 3, 1, 4}, {5, 3, 4, 2, 1, 0});
}

BOOST_AUTO_TEST_CASE( test7x7_1 ) {
    testANTS({2, 3, 0, 1, 6, 4, 5}, {3, 5, 0, 4, 1, 2, 6}, {5, 4, 3, 0, 6, 1, 2});
}

BOOST_AUTO_TEST_CASE( test8x8_1 ) {
    testANTS({0, 1, 4, 2, 3, 7, 5, 6}, {1, 0, 2, 5, 3, 4, 6, 7}, {1, 0, 5, 2, 3, 7, 4, 6});
}