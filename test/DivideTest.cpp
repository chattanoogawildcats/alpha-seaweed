#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE DivideTest
#include <boost/test/included/unit_test.hpp>
#include "../src/Divide.hpp"

BOOST_AUTO_TEST_CASE( testBuildPa ) {
	permutation expected = {0,1,4,2,3,5};
	permutation actual = Divide::buildPa("a","abca").toPermutation();
	BOOST_CHECK_EQUAL_COLLECTIONS(actual.begin(),actual.end(),expected.begin(),expected.end());

 	expected = {0,1,3,2,5,4};
	actual = Divide::buildPa("a","abac").toPermutation();
	BOOST_CHECK_EQUAL_COLLECTIONS(actual.begin(),actual.end(),expected.begin(),expected.end());

 	expected = {0,1,4,2,3,7,5,6};
	actual = Divide::buildPa("a","abbacb").toPermutation();
	BOOST_CHECK_EQUAL_COLLECTIONS(actual.begin(),actual.end(),expected.begin(),expected.end());
}

BOOST_AUTO_TEST_CASE( testBuildPb ) {
    permutation expected = {1,0,4,2,3,5};
    permutation actual = Divide::buildPb("b","abca").toPermutation();
    BOOST_CHECK_EQUAL_COLLECTIONS(actual.begin(),actual.end(),expected.begin(),expected.end());

    expected = {1,0,4,2,3,5};
    actual = Divide::buildPb("b","abac").toPermutation();
    BOOST_CHECK_EQUAL_COLLECTIONS(actual.begin(),actual.end(),expected.begin(),expected.end());

	expected = {1,0,2,5,3,4,6,7};
	actual = Divide::buildPb("b","abbacb").toPermutation();
	BOOST_CHECK_EQUAL_COLLECTIONS(actual.begin(),actual.end(),expected.begin(),expected.end());
}

// Now implemented!
BOOST_AUTO_TEST_CASE( testImplicitMultiply ) {
	permutation expected = {1,0,4,3,2,5};
	permutation a = {0,1,4,2,3,5};
	permutation b = {1,0,4,2,3,5};
	permutation actual = Divide::implicitMultiply(a,b).toPermutation();
	BOOST_CHECK_EQUAL_COLLECTIONS(actual.begin(),actual.end(),expected.begin(),expected.end());
	
	a = {0,2,1,5,3,4,6};
	b = {0,3,1,2,4,5,6};
	expected = {0,3,1,5,2,4,6};
	actual = Divide::implicitMultiply(a,b).toPermutation();
	BOOST_CHECK_EQUAL_COLLECTIONS(actual.begin(),actual.end(),expected.begin(),expected.end());
}

//Now needs new cases
BOOST_AUTO_TEST_CASE( testLongestCommonSubsequence ) {
    uint score = 3;
    permutation pc = {0,3,1,5,2,4,6};
    BOOST_CHECK_EQUAL(score, Divide::LongestCommonSubsequence(pc, 4));

}

//Needs a good example
// BOOST_AUTO_TEST_CASE( testAnts ) {
// 	permutation expected = {};
// 	permutation pc_hi = {};
// 	permutation pc_lo = {};
// 	permutation actual = Divide::ants(pc_lo,pc_hi);
// 	BOOST_CHECK_EQUAL_COLLECTIONS(actual.begin(),actual.end(),expected.begin(),expected.end());
// }
