#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE BraidTest
#include <boost/test/unit_test.hpp>

#include "../src/Braid.hpp"

/*
BOOST_AUTO_TEST_CASE( testTwisted ) {
    // As we just want to test the twisted function, we don't need legit braids, just to know the string's lengths
    Braid braid("AAAAAAA", "AAAAAAA"); 

    // Both from top
    BOOST_CHECK_EQUAL(braid.twisted(1, 2), true );
    BOOST_CHECK_EQUAL(braid.twisted(2, 1), false);

    // Both from left
    BOOST_CHECK_EQUAL(braid.twisted(9, 8), false );
    BOOST_CHECK_EQUAL(braid.twisted(8, 9), true  );

    // Top, left
    BOOST_CHECK_EQUAL(braid.twisted(9, 1), false );
    BOOST_CHECK_EQUAL(braid.twisted(1, 9), true);

}
*/

/*
 * List of tests to consider for each "tube" of seaweeds:
 * * Empty
 * * Empty with one on each side
 * * With only 1 item
 * * Full
 * * Full sans 1 on one side
 *
*/

BOOST_AUTO_TEST_CASE( testAFullBSub ) {
    Braid braid("AAAAAAA", "AABBBAA");

    // Full string, 3 middle chars
    BOOST_CHECK_EQUAL(braid.AFullBSub(0, 7), 3);
    BOOST_CHECK_EQUAL(braid.AFullBSub(2, 5), 3);

    // Start, End
    BOOST_CHECK_EQUAL(braid.AFullBSub(0, 4), 2);
    BOOST_CHECK_EQUAL(braid.AFullBSub(4, 7), 1);


    Braid braid2("ABBA", "CBBC");

    // Full string, then middle 2
    BOOST_CHECK_EQUAL(braid2.AFullBSub(0, 4), 2);
    BOOST_CHECK_EQUAL(braid2.AFullBSub(1, 3), 0);

    // One either side, both empty
    BOOST_CHECK_EQUAL(braid2.AFullBSub(1, 2), 0);
    BOOST_CHECK_EQUAL(braid2.AFullBSub(2, 3), 0);

    // Full but one on each side
    BOOST_CHECK_EQUAL(braid2.AFullBSub(0, 2), 1);
    BOOST_CHECK_EQUAL(braid2.AFullBSub(2, 4), 1);

    // Just one item
    BOOST_CHECK_EQUAL(braid2.AFullBSub(0, 1), 1);
    BOOST_CHECK_EQUAL(braid2.AFullBSub(3, 4), 1);

    Braid braid3("ABCDEFGCB", "ACBD");

    BOOST_CHECK_EQUAL(braid3.AFullBSub(0, 9), 6);
}
