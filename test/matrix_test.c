#include "../src/matrix.h"

void runTest(int size, int A[], int B[], int C[]) {
    /* Example B matrices are row->column, so transpose B for column->row indexing */
    int transposeB[size];
    for (int i = 0; i < size; i++)
    {
        transposeB[B[i]] = i;
    }

    /* Run the test */
    int result[size];
    multiply(size, A, transposeB, result);

    /* Compare against C */
    for (int i = 0; i < size; i++)
    {
        if (result[i] != C[i])
        {
            printf("FAIL\n");

            /* Print A */
            printf("A\n");
            for (int i = 0; i < size; i++)
            {
                printf("%i ", A[i]);
            }
            printf("\n");
            printf("\n");

            /* Print B (non-transposed) */
            printf("B (printing in row->column form)\n");
            for (int i = 0; i < size; i++)
            {
                printf("%i ", B[i]);
            }
            printf("\n");
            printf("\n");

            /* Print result */
            printf("Result (what we got)\n");
            for (int i = 0; i < size; i++)
            {
                printf("%i ", result[i]);
            }
            printf("\n");
            printf("\n");

            /* Print C */
            printf("C (what we should have got)\n");
            for (int i = 0; i < size; i++)
            {
                printf("%i ", C[i]);
            }
            printf("\n");
            printf("\n");

            /* Bail */
            exit(FAILING_TEST);
        }
    }
}

void test1_1() {
    int A[] = {0};
    int B[] = {0};
    int C[] = {0};
    runTest(1, A, B, C);

}
void test2_1() {
    int A[] = {1, 0};
    int B[] = {1, 0};
    int C[] = {1, 0};
    runTest(2, A, B, C);
}

void test2_2() {
    int A[] = {0, 1};
    int B[] = {1, 0};
    int C[] = {1, 0};
    runTest(2, A, B, C);
}

void test2_3() {
    int A[] = {1, 0};
    int B[] = {0, 1};
    int C[] = {1, 0};
    runTest(2, A, B, C);
}

void test2_4() {
    int A[] = {0, 1};
    int B[] = {0, 1};
    int C[] = {0, 1};
    runTest(2, A, B, C);
}

void test3_1() {
    int A[] = {2, 1, 0};
    int B[] = {2, 1, 0};
    int C[] = {2, 1, 0};
    runTest(3, A, B, C);
}

void test3_2() {
    int A[] = {1, 0, 2};
    int B[] = {2, 1, 0};
    int C[] = {2, 1, 0};
    runTest(3, A, B, C);
}

void test3_3() {
    int A[] = {0, 1, 2};
    int B[] = {0, 1, 2};
    int C[] = {0, 1, 2};
    runTest(3, A, B, C);
}

void test3_4() {
    int A[] = {1, 0, 2};
    int B[] = {1, 0, 2};
    int C[] = {1, 0, 2};
    runTest(3, A, B, C);
}

void test3_5() {
    int A[] = {2, 0, 1};
    int B[] = {1, 0, 2};
    int C[] = {2, 1, 0};
    runTest(3, A, B, C);
}

void test3_6() {
    int A[] = {1, 0, 2};
    int B[] = {2, 0, 1};
    int C[] = {2, 0, 1};
    runTest(3, A, B, C);
}

void test4_1() {
    int A[] = {1, 3, 0, 2};
    int B[] = {3, 0, 2, 1};
    int C[] = {3, 2, 0, 1};
    runTest(4, A, B, C);
}

void test5_1() {
    int A[] = {0, 4, 1, 3, 2};
    int B[] = {4, 3, 2, 1, 0};
    int C[] = {4, 3, 2, 1, 0};
    runTest(5, A, B, C);
}

void test5_2() {
    int A[] = {2, 1, 4, 0, 3};
    int B[] = {3, 4, 0, 2, 1};
    int C[] = {4, 3, 2, 0, 1};
    runTest(5, A, B, C);
}

void test6_1() {
    int A[] = {3, 2, 5, 4, 0, 1};
    int B[] = {5, 0, 2, 3, 1, 4};
    int C[] = {5, 3, 4, 2, 1, 0};
    runTest(6, A, B, C);
}

void test7_1() {
    int A[] = {2, 3, 0, 1, 6, 4, 5};
    int B[] = {3, 5, 0, 4, 1, 2, 6};
    int C[] = {5, 4, 3, 0, 6, 1, 2};
    runTest(7, A, B, C);
}

void test8_1() {
    int A[] = {0, 1, 4, 2, 3, 7, 5, 6};
    int B[] = {1, 0, 2, 5, 3, 4, 6, 7};
    int C[] = {1, 0, 5, 2, 3, 7, 4, 6};
    runTest(8, A, B, C);
}

void runTests() {
    test1_1();

    test2_1();
    test2_2();
    test2_3();
    test2_4();

    test3_1();
    test3_2();
    test3_3();
    test3_4();
    test3_5();

    test4_1();

    test5_1();
    test5_2();

    test6_1();

    test7_1();

    test8_1();
}